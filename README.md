### Cофт ###

* Apache 2.4
* MySQL 5.7
* PHP ^7.2

### Библиотеки ###

* Cм. composer.json (composer.lock для подробной информации)

### Установка ###

* Клонируем репозитарий
* composer install
* cp .env.example .env
* в .env прописываем подключение к бд
* php dbseed.php - импорт тестовых данных в бд

  

### Краткое описание API ###
API доступно в 2 форматах:
 - json
 - xml

Сущности:

 - Employe - сотрудник
 - Department - отдел

### Методы сущности Employe:

 **Получить список сотрудников:**

GET /api/employes.{format} - возвращает сотрудников. Доступна
пагинация через GET параметры offset, limit

Пример json:

GET /api/employes.json?offset=0&limit=3

```json
{
"success": true,
"data": [
	{
	"id": 1,
	"firstname": "Krasimir",
	"lastname": "Hristozov",
	"department_id": 1
	},
	{
	"id": 2,
	"firstname": "Maria",
	"lastname": "Petrova",
	"department_id": 2
	},
	{
	"id": 3,
	"firstname": "Masha",
	"lastname": "Ivanova",
	"department_id": 3
	}
],
	"errors": []
}
```
Пример xml:

GET /api/employes.xml?offset=0&limit=3

```xml
<?xml version="1.0"?>
<root>
	<success>1</success>
	<data>
		<item>
			<id>1</id>
			<firstname>Krasimir</firstname>
			<lastname>Hristozov</lastname>
			<department_id>1</department_id>
			</item>
		<item>
			<id>2</id>
			<firstname>Maria</firstname>
			<lastname>Petrova</lastname>
			<department_id>2</department_id>
		</item>
		<item>
			<id>3</id>
			<firstname>Masha</firstname>
			<lastname>Ivanova</lastname>
			<department_id>3</department_id>
		</item>
	</data>
	<errors/>
</root>
```
**Получить сотрудника по его id:**

GET /api/employes/{id}.{format} - возвращает сотрудника с указаным id(
параметр {id} в ури).

**Добавить сотрудника:**

POST /api/employes.{format}

Пример тела запроса json:

```json
{
	"firstname": "Krasimir",
	"lastname": "Hristozov",
	"department_id": 1
}
```
Ответ:

```json
{
	"success": true,
	"data": {
		"firstname": "Krasimir",
		"lastname": "Hristozov",
		"department_id": 1,
		"id": 19
	},
	"errors": []
}
```
Пример тела запроса xml:

```xml
<?xml version="1.0"?>
<employe>
	<firstname>Krasimir</firstname>
	<lastname>Hristozov</lastname>
	<department_id>1</department_id>
</employe>
```
Ответ:

```xml
<?xml version="1.0"?>
<root>
	<success>1</success>
	<data>
		<firstname>Krasimir</firstname>
		<lastname>Hristozov</lastname>
		<department_id>1</department_id>
		<id>20</id>
	</data>
	<errors/>
</root>
```
**Изменение сотрудника:**

PUT /api/employes/{id}.{format}

Пример тела запроса json:

```json
{
	"firstname": "Krasimir",
	"lastname": "Hristozov",
	"department_id": 1
}
```
Пример тела запроса xml:
```xml
<?xml version="1.0"?>
<employe>
	<firstname>Alex</firstname>
	<lastname>Test</lastname>
	<department_id>8</department_id>
</employe>
```
**Удаление сотрудника:**

DELETE /api/employes/{id}.{format} 

### Методы сущности Department:

**Получить список отделов:**

GET /api/departments.{format} - возвращает отделы. Доступна
пагинация через GET параметры offset, limit

Пример json:

GET /api/departments.json?offset=0&limit=3

```json
{
	"success": true,
	"data": [
		{
			"id": 1,
			"name": "A",
			"parent_id": null
		},
		{
			"id": 2,
			"name": "B",
			"parent_id": 1
		},
		{
			"id": 3,
			"name": "C",
			"parent_id": 2
		}
	],
	"errors": []
}
```
Пример xml:

GET /api/departments.xml?offset=0&limit=3

```xml
<?xml  version="1.0"?>
<root>
	<success>1</success>
	<data>
		<item>
			<id>1</id>
			<name>A</name>
			<parent_id/>
		</item>
		<item>
			<id>2</id>
			<name>B</name>
			<parent_id>1</parent_id>
		</item>
		<item>
			<id>3</id>
			<name>C</name>
			<parent_id>2</parent_id>
		</item>
	</data>
	<errors/>
</root>
```
**Получить отдел по его id:**

GET /api/departments/{id}.{format} - возвращает отдел с указаным id(
параметр {id} в ури).

**Добавить отдел :**

POST /api/departments.{format}

Пример тела запроса json:

```json
{
	"name": "Department A",
	"parent_id": 1
}
```
Ответ:

```json
{
	"success": true,
	"data": {
		"name": "Department A",
		"parent_id": 1,
		"id": 2
	},
	"errors": []
}

```
Пример тела запроса xml:

```xml
<?xml version="1.0"?>
<department>
	<name>Department A</name>
	<parent_id>1</parent_id>
</department>
```
Ответ:

```xml
<?xml version="1.0"?>
<root>
	<success>1</success>
	<data>
		<name>Department A</name>
		<parent_id>1</parent_id>
		<id>2</id>
	</data>
	<errors/>
</root>
```
**Изменение отдела:**

PUT /api/departments/{id}.{format}

Пример тела запроса json:

```json
{
	"name": "Department B",
	"parent_id": 1
}
```
Пример тела запроса xml:
```xml
<?xml version="1.0"?>
<department>
	<name>Department B</name>
	<parent_id>1</parent_id>
</department>
```
**Удаление сотрудника:**

DELETE /api/departments/{id}.{format} 

**Получить(экспортировать в файл) иерархическую структуру отдела:**

GET /api/departments/{id}/structure.{format} 
Поддерживает GET параметр file. Если задать file=true, то вернет почти такую же структуру данных, но в заголовках будет информация о файле и его размере. Если запрос выполнить в браузере, то откроется диалоговое окно на скачивание.

Пример в json:

```json
{
	"success": true,
	"data": [
		{
			"id": 10,
			"name": "C",
			"parent_id": 9,
			"sub_departments": [
				{
					"id": 11,
					"name": "D",
					"parent_id": 10,
					"sub_departments": [
						{
							"id": 12,
							"name": "E",
							"parent_id": 11,
							"sub_departments": [],
							"employes": [
								{
									"id": 15,
									"firstname": "Richard",
									"lastname": "Smith",
									"department_id": 12
								},
								{
									"id": 16,
									"firstname": "Anna",
									"lastname": "Henderson",
									"department_id": 12
								}
							]
						}
					],
					"employes": [
						{
							"id": 14,
							"firstname": "Jane",
							"lastname": "Smith",
							"department_id": 11
						}
					]
				}
			],
			"employes": [
				{
					"id": 12,
					"firstname": "Masha",
					"lastname": "Ivanova",
					"department_id": 10
				},
				{
					"id": 13,
					"firstname": "Josh",
					"lastname": "Sidorov",
					"department_id": 10
				}
			]
		}
	],
	"errors": []
}
```
Пример в xml:
```xml
<?xml  version="1.0"?>
<root>
	<success>1</success>
	<data>
		<item>
			<id>10</id>
			<name>C</name>
			<parent_id>9</parent_id>
			<sub_departments>
				<item>
					<id>11</id>
					<name>D</name>
					<parent_id>10</parent_id>
					<sub_departments>
						<item>
							<id>12</id>
							<name>E</name>
							<parent_id>11</parent_id>
							<sub_departments/>
							<employes>
								<item>
									<id>15</id>
									<firstname>Richard</firstname>
									<lastname>Smith</lastname>
									<department_id>12</department_id>
								</item>
								<item>
									<id>16</id>
									<firstname>Anna</firstname>
									<lastname>Henderson</lastname>
									<department_id>12</department_id>
								</item>
							</employes>
						</item>
					</sub_departments>
					<employes>
						<item>
							<id>14</id>
							<firstname>Jane</firstname>
							<lastname>Smith</lastname>
							<department_id>11</department_id>
						</item>
					</employes>
				</item>
			</sub_departments>
			<employes>
				<item>
					<id>12</id>
					<firstname>Masha</firstname>
					<lastname>Ivanova</lastname>
					<department_id>10</department_id>
				</item>
				<item>
				<id>13</id>
					<firstname>Josh</firstname>
					<lastname>Sidorov</lastname>
					<department_id>10</department_id>
				</item>
			</employes>
		</item>
	</data>
	<errors/>
</root>
```
**Получить(экспортировать в файл) иерархическую структуру организации:**

GET /api/departments/all/structure.{format} 
Поддерживает GET параметр file. Если задать file=true, то вернет почти такую же структуру данных, но в заголовках будет информация о файле и его размере. Если запрос выполнить в браузере, то откроется диалоговое окно на скачивание.

**Импортировать структуру отдела с сотрудниками:**
POST /api/departments/{id}/import.{format} 

Пример тела запроса в json:
```json
{
	"file": "ewoJIm5hbWUiOiAiQyIsCgkic3ViX2RlcGFydG1lbnRzIjogWwoJCXsKCQkJIm5hbWUiOiAiRCIsCgkJCSJzdWJfZGVwYXJ0bWVudHMiOiBbCgkJCQl7CgkJCQkJIm5hbWUiOiAiRSIsCgkJCQkJInN1Yl9kZXBhcnRtZW50cyI6IFtdLAoJCQkJCSJlbXBsb3llcyI6IFsKCQkJCQkJewoJCQkJCQkJImZpcnN0bmFtZSI6ICJSaWNoYXJkIiwKCQkJCQkJCSJsYXN0bmFtZSI6ICJTbWl0aCIKCQkJCQkJfSwKCQkJCQkJewoJCQkJCQkJImZpcnN0bmFtZSI6ICJBbm5hIiwKCQkJCQkJCSJsYXN0bmFtZSI6ICJIZW5kZXJzb24iCgkJCQkJCX0KCQkJCQldCgkJCQl9CgkJCV0sCgkJCSJlbXBsb3llcyI6IFsKCQkJCXsKCQkJCQkiZmlyc3RuYW1lIjogIkphbmUiLAoJCQkJCSJsYXN0bmFtZSI6ICJTbWl0aCIKCQkJCX0KCQkJXQoJCX0KCV0sCgkiZW1wbG95ZXMiOiBbCgkJewoJCQkiZmlyc3RuYW1lIjogIk1hc2hhIiwKCQkJImxhc3RuYW1lIjogIkl2YW5vdmEiCgkJfSwKCQl7CgkJCSJmaXJzdG5hbWUiOiAiSm9zaCIsCgkJCSJsYXN0bmFtZSI6ICJTaWRvcm92IgoJCX0KCV0KfQ=="
}
```
где file - base64 строка файла импорта(можно использовать файл экспорта, связи будут строится заново от родителя(отдела или организации), в который выполняется импорт ) в json. Пример json файла:
```json
{
	"name": "C",
	"sub_departments": [
		{
			"name": "D",
			"sub_departments": [
				{
					"name": "E",
					"sub_departments": [],
					"employes": [
						{
							"firstname": "Richard",
							"lastname": "Smith"
						},
						{
							"firstname": "Anna",
							"lastname": "Henderson"
						}
					]
				}
			],
			"employes": [
				{
					"firstname": "Jane",
					"lastname": "Smith"
				}
			]
		}
	],
	"employes": [
		{
			"firstname": "Masha",
			"lastname": "Ivanova"
		},
		{
			"firstname": "Josh",
			"lastname": "Sidorov"
		}
	]
}
```
Пример тела запроса в xml:
```xml
<?xml version="1.0"?>
<import>
	<file>PD94bWwgdmVyc2lvbj0iMS4wIj8+Cjxyb290PgogICAgPGl0ZW0+CiAgICAgICAgPGlkPjE8L2lkPgogICAgICAgIDxuYW1lPkE8L25hbWU+CiAgICAgICAgPHBhcmVudF9pZC8+CiAgICAgICAgPHN1Yl9kZXBhcnRtZW50cz4KICAgICAgICAgICAgPGl0ZW0+CiAgICAgICAgICAgICAgICA8aWQ+MjwvaWQ+CiAgICAgICAgICAgICAgICA8bmFtZT5CPC9uYW1lPgogICAgICAgICAgICAgICAgPHBhcmVudF9pZD4xPC9wYXJlbnRfaWQ+CiAgICAgICAgICAgICAgICA8c3ViX2RlcGFydG1lbnRzPgogICAgICAgICAgICAgICAgICAgIDxpdGVtPgogICAgICAgICAgICAgICAgICAgICAgICA8aWQ+MzwvaWQ+CiAgICAgICAgICAgICAgICAgICAgICAgIDxuYW1lPkM8L25hbWU+CiAgICAgICAgICAgICAgICAgICAgICAgIDxwYXJlbnRfaWQ+MjwvcGFyZW50X2lkPgogICAgICAgICAgICAgICAgICAgICAgICA8c3ViX2RlcGFydG1lbnRzPgogICAgICAgICAgICAgICAgICAgICAgICAgICAgPGl0ZW0+CiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPGlkPjQ8L2lkPgogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxuYW1lPkQ8L25hbWU+CiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPHBhcmVudF9pZD4zPC9wYXJlbnRfaWQ+CiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPHN1Yl9kZXBhcnRtZW50cz4KICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPGl0ZW0+CiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8aWQ+NjwvaWQ+CiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8bmFtZT5FPC9uYW1lPgogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPHBhcmVudF9pZD40PC9wYXJlbnRfaWQ+CiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8c3ViX2RlcGFydG1lbnRzLz4KICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxlbXBsb3llcz4KICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8aXRlbT4KICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPGlkPjY8L2lkPgogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8Zmlyc3RuYW1lPlJpY2hhcmQ8L2ZpcnN0bmFtZT4KICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPGxhc3RuYW1lPlNtaXRoPC9sYXN0bmFtZT4KICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPGRlcGFydG1lbnRfaWQ+NjwvZGVwYXJ0bWVudF9pZD4KICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L2l0ZW0+CiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPGl0ZW0+CiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxpZD45PC9pZD4KICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPGZpcnN0bmFtZT5Bbm5hPC9maXJzdG5hbWU+CiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxsYXN0bmFtZT5IZW5kZXJzb248L2xhc3RuYW1lPgogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8ZGVwYXJ0bWVudF9pZD42PC9kZXBhcnRtZW50X2lkPgogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvaXRlbT4KICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvZW1wbG95ZXM+CiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvaXRlbT4KICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L3N1Yl9kZXBhcnRtZW50cz4KICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8ZW1wbG95ZXM+CiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxpdGVtPgogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPGlkPjQ8L2lkPgogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPGZpcnN0bmFtZT5KYW5lPC9maXJzdG5hbWU+CiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8bGFzdG5hbWU+U21pdGg8L2xhc3RuYW1lPgogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPGRlcGFydG1lbnRfaWQ+NDwvZGVwYXJ0bWVudF9pZD4KICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9pdGVtPgogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvZW1wbG95ZXM+CiAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L2l0ZW0+CiAgICAgICAgICAgICAgICAgICAgICAgIDwvc3ViX2RlcGFydG1lbnRzPgogICAgICAgICAgICAgICAgICAgICAgICA8ZW1wbG95ZXM+CiAgICAgICAgICAgICAgICAgICAgICAgICAgICA8aXRlbT4KICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8aWQ+MzwvaWQ+CiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPGZpcnN0bmFtZT5NYXNoYTwvZmlyc3RuYW1lPgogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxsYXN0bmFtZT5JdmFub3ZhPC9sYXN0bmFtZT4KICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8ZGVwYXJ0bWVudF9pZD4zPC9kZXBhcnRtZW50X2lkPgogICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9pdGVtPgogICAgICAgICAgICAgICAgICAgICAgICAgICAgPGl0ZW0+CiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPGlkPjg8L2lkPgogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxmaXJzdG5hbWU+Sm9zaDwvZmlyc3RuYW1lPgogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxsYXN0bmFtZT5TaWRvcm92PC9sYXN0bmFtZT4KICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8ZGVwYXJ0bWVudF9pZD4zPC9kZXBhcnRtZW50X2lkPgogICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9pdGVtPgogICAgICAgICAgICAgICAgICAgICAgICA8L2VtcGxveWVzPgogICAgICAgICAgICAgICAgICAgIDwvaXRlbT4KICAgICAgICAgICAgICAgIDwvc3ViX2RlcGFydG1lbnRzPgogICAgICAgICAgICAgICAgPGVtcGxveWVzPgogICAgICAgICAgICAgICAgICAgIDxpdGVtPgogICAgICAgICAgICAgICAgICAgICAgICA8aWQ+MjwvaWQ+CiAgICAgICAgICAgICAgICAgICAgICAgIDxmaXJzdG5hbWU+TWFyaWE8L2ZpcnN0bmFtZT4KICAgICAgICAgICAgICAgICAgICAgICAgPGxhc3RuYW1lPlBldHJvdmE8L2xhc3RuYW1lPgogICAgICAgICAgICAgICAgICAgICAgICA8ZGVwYXJ0bWVudF9pZD4yPC9kZXBhcnRtZW50X2lkPgogICAgICAgICAgICAgICAgICAgIDwvaXRlbT4KICAgICAgICAgICAgICAgIDwvZW1wbG95ZXM+CiAgICAgICAgICAgIDwvaXRlbT4KICAgICAgICAgICAgPGl0ZW0+CiAgICAgICAgICAgICAgICA8aWQ+NTwvaWQ+CiAgICAgICAgICAgICAgICA8bmFtZT5FPC9uYW1lPgogICAgICAgICAgICAgICAgPHBhcmVudF9pZD4xPC9wYXJlbnRfaWQ+CiAgICAgICAgICAgICAgICA8c3ViX2RlcGFydG1lbnRzLz4KICAgICAgICAgICAgICAgIDxlbXBsb3llcz4KICAgICAgICAgICAgICAgICAgICA8aXRlbT4KICAgICAgICAgICAgICAgICAgICAgICAgPGlkPjU8L2lkPgogICAgICAgICAgICAgICAgICAgICAgICA8Zmlyc3RuYW1lPkpvaG48L2ZpcnN0bmFtZT4KICAgICAgICAgICAgICAgICAgICAgICAgPGxhc3RuYW1lPkpvaG5zb248L2xhc3RuYW1lPgogICAgICAgICAgICAgICAgICAgICAgICA8ZGVwYXJ0bWVudF9pZD41PC9kZXBhcnRtZW50X2lkPgogICAgICAgICAgICAgICAgICAgIDwvaXRlbT4KICAgICAgICAgICAgICAgIDwvZW1wbG95ZXM+CiAgICAgICAgICAgIDwvaXRlbT4KICAgICAgICAgICAgPGl0ZW0+CiAgICAgICAgICAgICAgICA8aWQ+NzwvaWQ+CiAgICAgICAgICAgICAgICA8bmFtZT5HPC9uYW1lPgogICAgICAgICAgICAgICAgPHBhcmVudF9pZD4xPC9wYXJlbnRfaWQ+CiAgICAgICAgICAgICAgICA8c3ViX2RlcGFydG1lbnRzLz4KICAgICAgICAgICAgICAgIDxlbXBsb3llcz4KICAgICAgICAgICAgICAgICAgICA8aXRlbT4KICAgICAgICAgICAgICAgICAgICAgICAgPGlkPjc8L2lkPgogICAgICAgICAgICAgICAgICAgICAgICA8Zmlyc3RuYW1lPkRvbm5hPC9maXJzdG5hbWU+CiAgICAgICAgICAgICAgICAgICAgICAgIDxsYXN0bmFtZT5TbWl0aDwvbGFzdG5hbWU+CiAgICAgICAgICAgICAgICAgICAgICAgIDxkZXBhcnRtZW50X2lkPjc8L2RlcGFydG1lbnRfaWQ+CiAgICAgICAgICAgICAgICAgICAgPC9pdGVtPgogICAgICAgICAgICAgICAgPC9lbXBsb3llcz4KICAgICAgICAgICAgPC9pdGVtPgogICAgICAgIDwvc3ViX2RlcGFydG1lbnRzPgogICAgICAgIDxlbXBsb3llcz4KICAgICAgICAgICAgPGl0ZW0+CiAgICAgICAgICAgICAgICA8aWQ+MTwvaWQ+CiAgICAgICAgICAgICAgICA8Zmlyc3RuYW1lPktyYXNpbWlyPC9maXJzdG5hbWU+CiAgICAgICAgICAgICAgICA8bGFzdG5hbWU+SHJpc3Rvem92PC9sYXN0bmFtZT4KICAgICAgICAgICAgICAgIDxkZXBhcnRtZW50X2lkPjE8L2RlcGFydG1lbnRfaWQ+CiAgICAgICAgICAgIDwvaXRlbT4KICAgICAgICA8L2VtcGxveWVzPgogICAgPC9pdGVtPgo8L3Jvb3Q+</file>
</import>
```

где file - base64 строка файла импорта(можно использовать файл экспорта, связи будут строится заново от родителя(отдела или организации), в который выполняется импорт ) в xml. Пример xml файла:

```xml
<?xml  version="1.0"?>
<root>
	<item>
		<name>C</name>
		<sub_departments>
			<item>
				<name>D</name>
				<sub_departments>
					<item>
						<name>E</name>
						<sub_departments/>
						<employes>
							<item>
								<firstname>Richard</firstname>
								<lastname>Smith</lastname>
							</item>
							<item>
								<firstname>Anna</firstname>
								<lastname>Henderson</lastname>
							</item>
						</employes>
					</item>
				</sub_departments>
				<employes>
					<item>
						<firstname>Jane</firstname>
						<lastname>Smith</lastname>
					</item>
				</employes>
			</item>
		</sub_departments>
		<employes>
			<item>
				<firstname>Masha</firstname>
				<lastname>Ivanova</lastname>
			</item>
			<item>
				<firstname>Josh</firstname>
				<lastname>Sidorov</lastname>
			</item>
		</employes>
	</item>
</root>
```