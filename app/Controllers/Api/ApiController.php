<?php
namespace App\Controllers\Api;

use App\Response\Response;
use App\Request\Request;
use App\Import\EntityImporter;

abstract class ApiController {

    /**
     * Max rows per request
     */
    const MAX_LIMIT = 250;

    /**
     * Default rows per request
     */
    const DEFAULT_LIMIT = 25;

    /**
     * Entity model class
     */
    protected $modelClass;

    /**
     * @var Rakit\Validation\Validator
     */
    protected $validator;

    /**
     * @var array
     */
    protected $data = [];

    /**
     * @var array
     */
    protected $errors = [];

    /**
     * Status code of response
     * @var integer
     */
    protected $code = 200;

    public function __construct()
    {
        $this->validator = new \Rakit\Validation\Validator();
    }

    /**
     * Get list of entities
     * @param string $format
     * @return string
     */
    public function index($format)
    {
        $limit = isset($_GET['limit']) && intval($_GET['limit']) ? $_GET['limit'] : self::DEFAULT_LIMIT;
        $limit = $limit > self::MAX_LIMIT ? self::MAX_LIMIT : $limit;
        $offset = isset($_GET['offset']) && intval($_GET['offset']) ? $_GET['offset'] : 0;
        $entities = $this->modelClass::limit($limit)->offset($offset)->get();
        $response = Response::getResponseTypeByFormat($format);
        return $response->send($entities->toArray(), [], $this->code);
    }

    /**
     * Get entity by id
     * @param integer $id
     * @param string $format
     * @return string
     */
    public function show($id, $format)
    {
        $entity = $this->modelClass::find($id);
        $response = Response::getResponseTypeByFormat($format);
        if ($entity) {
            $this->data = $entity->toArray();
        } else {
            $this->errors[] = [
                'database' => 'Entity with id='.$id.' not found'
            ];
            $this->code = 404;
        }
        return $response->send($this->data, $this->errors, $this->code);
    }

    /**
     * Create entity
     * @return string
     */
    public function create($format)
    {
        $request = Request::getRequestTypeByFormat($format);
        $response = Response::getResponseTypeByFormat($format);
        $input = $request->toArray(file_get_contents('php://input'));
        EntityImporter::import(
            $this->modelClass,
            $input,
            $this->data,
            $this->errors,
            $this->code,
            EntityImporter::ONLY_CREATE
        );
        return $response->send($this->data, $this->errors, $this->code);
    }

    /**
     * Update entity
     * @return string
     */
    public function update($id, $format)
    {
        $request = Request::getRequestTypeByFormat($format);
        $response = Response::getResponseTypeByFormat($format);
        $input = $request->toArray(file_get_contents('php://input'));
        $input['id'] = $id;
        EntityImporter::import(
            $this->modelClass,
            $input,
            $this->data,
            $this->errors,
            $this->code,
            EntityImporter::ONLY_UPDATE
        );
        return $response->send($this->data, $this->errors, $this->code);

    }

    /**
     * Destroy entity
     * @return string
     */
    public function destroy($id, $format)
    {
        $response = Response::getResponseTypeByFormat($format);
        $entityName = getEntityNameByClass($this->modelClass);
        $entity = $this->modelClass::find($id);
        if ($entity) {
            $entity->delete();
            $this->code = 204;
        } else {
            $this->errors = [
                'database' => 'Entity with id='.$id.' not found'
            ];
            $this->code = 422;
        }
        return $response->send($this->data, $this->errors, $this->code);
    }

}