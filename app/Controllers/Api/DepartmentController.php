<?php
namespace App\Controllers\Api;

use App\Response\Response;
use App\Request\Request;
use App\Models\Department;
use App\Models\Employe;

class DepartmentController extends ApiController {

    /**
     * Entity model class
     */
    protected $modelClass = Department::class;

    /**
     * Get department structure
     * @param string $id
     * @param string $format
     * @return string
     */
    public function structure($id, $format)
    {
        $id = $id == 'all' ? null : intval($id);
        $isFile = $_GET['file'] ?? false;
        $request = Request::getRequestTypeByFormat($format);
        $response = Response::getResponseTypeByFormat($format);
        $departmentsEntity = Department::with('subDepartments');
        $departments = [];
        $mainDepartment = [];

        //get department hierarchy
        if ($id) {
            $departments = $departmentsEntity->where('parent_id', $id)->get();
            $mainDepartment = Department::find($id);
        } else {
            $departments = $departmentsEntity->whereNull('parent_id')->get();
        }
        if ($departments) {
            $departmentsHierarchy = $departments->toArray();

            //if set department id, need to add this department in top hierarchy
            if ($mainDepartment) {
                $mainDepartment = $mainDepartment->toArray();
                $mainDepartment['sub_departments'] = $departmentsHierarchy;
                $departmentsHierarchy = [$mainDepartment];
            }
            $departmentOneDemArray = [];

            //form one dimensional array from hierarchy
            \App\Helpers\HierarchyHelper::hierarchyToArray(
                $departmentsHierarchy,
                'sub_departments', 
                $departmentOneDemArray
            );
            
            //select all employes in main(in query) department
            $employes = Employe::whereIn('department_id', array_column($departmentOneDemArray, 'id'))->get();
            $employesArray = $employes->toArray();
            
            //add employes to departments hierarchy
            \App\Helpers\HierarchyHelper::addChildrenToHierarchy(
                $departmentsHierarchy,
                $employesArray,
                'sub_departments',
                'department_id',
                'employes'
            );
            $this->data = $departmentsHierarchy;
            $this->code = 200;
        } else {
            $this->errors = [
                'common' => 'Department not found'
            ];
            $this->code = 404;
        }
        if ($isFile && !$this->errors) {
            return $response->file($this->data);
        }
        return $response->send($this->data, $this->errors, $this->code);
    }

    /**
     * Import department structure
     * @param string $id
     * @param string $format
     * @return string
     */
    public function import($id, $format)
    {
        $id = $id == 'all' ? null : intval($id);
        $request = Request::getRequestTypeByFormat($format);
        $response = Response::getResponseTypeByFormat($format);
        $inputs = $request->toArray(file_get_contents('php://input'));
        if ($inputs) {
            $validation = $this->validator->make($inputs, [
                'file' => 'required'
            ]);
            $validation->validate();
            if ($validation->fails()) {
                $this->errors = $validation->errors()->toArray();
                $this->code = 422;
            } else {
                $file = base64_decode($inputs['file']);
                $file = $request->toArray($file);
                if (count($file)) {
                    
                    //get department hierarchy
                    $departmentHierarchy = $file;
                    if ($id !== null && $id) {
                        $mainDepartment = Department::find($id);
                        if (!$mainDepartment) {
                            $this->errors[] = [
                                'file' => 'Department with id='.$id.' not found'
                            ];
                            $this->code = 422;
                        }
                    }
                    if (empty($this->errors)) {
                        
                        //related models with main model in this hierarchy
                        $relatedModels = [
                            'employes' => [
                                'model' => \App\Models\Employe::class,
                                'key' => 'department_id'
                            ]
                        ];

                        //run import
                        \App\Import\HierarchyImporter::import(
                            $departmentHierarchy,
                            $this->modelClass,
                            'parent_id',
                            $id,
                            $relatedModels,
                            'sub_departments',
                            $this->validator,
                            $this->errors,
                            $this->code
                        );
                    }

                } else {
                    $this->errors[] = [
                        'file' => 'Wrong file structure.'
                    ];
                    $this->code = 422;
                }
            }
        } else {
            $this->errors[] = [
                'inputs' => 'Inputs data empty or has wrong format.'
            ];
            $this->code = 422;
        }
        return $response->send($this->data, $this->errors, $this->code);
    }

}