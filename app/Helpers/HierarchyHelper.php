<?php
namespace App\Helpers;

class HierarchyHelper {

    /**
     * Form one dimensional array from hierarchy
     * @param array $hierarchy
     * @param string $recursiveKey
     * @param array $result
     */
    public static function hierarchyToArray($hierarchy, $recursiveKey, &$result)
    {
        foreach ($hierarchy as $item) {
            $addedItem = $item;
            if (isset($addedItem[$recursiveKey])) {
                unset($addedItem[$recursiveKey]);
            }
            $result[] = $addedItem;
            if (isset($item[$recursiveKey]) && count($item[$recursiveKey]) > 0) {
                self::hierarchyToArray($item[$recursiveKey], $recursiveKey, $result);
            }
        }
    }

    /**
     * Add children to hierarchy
     * @param array $hierarchy
     * @param array $children
     * @param string $recursiveKey
     * @param string $tieKey
     * @param string $childHierarchyKeyName
     */
    public static function addChildrenToHierarchy(&$hierarchy, $children, $recursiveKey, $tieKey, $childHierarchyKeyName)
    {
        foreach ($hierarchy as &$hitem) {
            if (!isset($hitem[$childHierarchyKeyName])) {
                $hitem[$childHierarchyKeyName] = [];
            }
            foreach ($children as $child) {
                if ($hitem['id'] == $child[$tieKey]) {
                    $hitem[$childHierarchyKeyName][] = $child;
                }
            }
            if (isset($hitem[$recursiveKey]) && count($hitem[$recursiveKey]) > 0) {
                self::addChildrenToHierarchy(
                    $hitem[$recursiveKey],
                    $children,
                    $recursiveKey,
                    $tieKey,
                    $childHierarchyKeyName
                );
            }
        }
    }
}