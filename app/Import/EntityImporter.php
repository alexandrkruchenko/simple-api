<?php
namespace App\Import;

class EntityImporter {

    const ONLY_CREATE = 1;
    const ONLY_UPDATE = 2;
    const CREATE_OR_UPDATE = 3;

    /**
     * Import entity to db
     * @param array $model
     * @param $input
     * @param $data
     * @param $errors
     * @param int $code
     * @param int $flag
     * @return void $errors
     */
    public static function import(
        $model, 
        $input, 
        &$data, 
        &$errors,
        $code = 200,
        $flag = self::ONLY_CREATE
    ) {
        
        $entityName = getEntityNameByClass($model);
        if ($input) {
            $validator = new \Rakit\Validation\Validator();
            //validate input
            $validation = $validator->make(
                $input,
                ($flag == self::ONLY_UPDATE ? $model::$onUpdateValidationRules :
                    $model::$onCreateValidationRules)
            );
            $validation->validate();
            if ($validation->fails()) {
                $error = [
                    'error' => $validation->errors()->toArray()
                ];
                $errors[] = $error;
            } else {

                //validate relations
                $relationErrors = [];
                $relations = $model::$relationsForValidation;
                if ($relations) {
                    $relationErrors = $model::validateRelations($input, $relations);
                }
                if ($relationErrors) {
                    $error = [
                        'error' => $relationErrors
                    ];
                    $errors[] = $error;
                } else {
                    switch ($flag) {
                        case self::ONLY_CREATE:
                            unset($input['id']);
                            $entity = $model::create($input);
                        break;

                        case self::ONLY_UPDATE:
                            $id = 0;
                            if (isset($input['id'])) {
                                $id = $input['id'];
                                unset($input['id']);
                            }
                            $entity = $model::find($id);
                            if ($entity) {
                                foreach ($input as $ikey => $value) {
                                    if (in_array($ikey, $entity->fillable)) {
                                        $entity->{$ikey} = $value;
                                    }
                                }
                                $entity->save();
                            } else {
                                $error = [
                                    'error' => $entityName.' update error. '.$entityName.' with id='.$id.' non found'
                                ];
                                $errors[] = $error;
                            }
                        break;

                        case self::CREATE_OR_UPDATE:
                            $id = 0;
                            if (isset($input['id'])) {
                                $id = $input['id'];
                                unset($input['id']);
                            }
                            $entity = $model::updateOrCreate(
                                ['id' => $id],
                                $input
                            );
                        break;
                    }                        
                    $data = $entity->toArray();
                }
            }
        } else {
            $errors[] = [
                'inputs' => 'Inputs data empty or has wrong format'
            ];
        }
        if ($errors) {
            $code = 422;
        }
    }

}