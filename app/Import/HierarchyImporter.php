<?php
namespace App\Import;

use Rakit\Validation\Validator;

class HierarchyImporter {

    /**
     * Import hierarchy to db
     * @param array $hierarchy
     * @param string $modelClass
     * @param $parentKeyName
     * @param $parentKeyValue
     * @param array $relatedModels
     * @param string $recursiveKey
     * @param Validator $validator
     * @param array $errors
     * @param integer $code
     * @param integer $level
     */
    public static function import(
        $hierarchy,
        $modelClass,
        $parentKeyName,
        $parentKeyValue,
        $relatedModels,
        $recursiveKey,
        $validator,
        &$errors,
        $code = 200,
        $level = 1
    ) {
        $mainEntityName = getEntityNameByClass($modelClass);
        $index = 0;
        foreach ($hierarchy as $key => $hitem) {
            $mainEntityProperties = [];
            $subEntities = [];
            if (is_array($hitem)) {
                foreach ($hitem as $propertyName => $property) {
                    if (isset($relatedModels[$propertyName])) {
                        $subEntities[$propertyName] = [
                            'model' => $relatedModels[$propertyName]['model'],
                            'key' => $relatedModels[$propertyName]['key'],
                            'data' => $property
                        ];
                    } elseif ($propertyName != $recursiveKey) {
                        $mainEntityProperties[$propertyName] = $property;
                    }
                }
            }
            $validation = $validator->make(
                $mainEntityProperties,
                $modelClass::$onCreateValidationRules
            );
            $validation->validate();
            if ($validation->fails()) {
                $error = [
                    'index' => $index,
                    'level' => $level,
                    'entity' => $mainEntityName,
                    'error' => $validation->errors()->toArray()
                ];
                $errors[] = $error;
            } else {
                $mainEntityProperties[$parentKeyName] = $parentKeyValue;
                $mainEntity = $modelClass::create($mainEntityProperties);
                $parentKeyValue = $mainEntity->id;
                //add subEntities
                foreach ($subEntities as $se) {
                    $subEntityName = substr(strrchr($se['model'], "\\"), 1);
                    foreach ($se['data'] as $sekey => $seitem) {
                        $validation = $validator->make(
                            $seitem,
                            $se['model']::$onCreateValidationRules
                        );
                        $validation->validate();
                        if ($validation->fails()) {
                            $error = [
                                'index' => $index,
                                'subindex' => $sekey,
                                'level' => $level,
                                'entity' => $subEntityName,
                                'error' => $validation->errors()->toArray()
                            ];
                            $errors[] = $error;
                        } else {
                            $seitem[$se['key']] = $mainEntity->id;
                            $se['model']::create($seitem);
                        }
                    }
                }
            }
            if (isset($hitem[$recursiveKey]) && count($hitem[$recursiveKey]) > 0) {
                self::import(
                    $hitem[$recursiveKey],
                    $modelClass,
                    $parentKeyName,
                    $parentKeyValue,
                    $relatedModels,
                    $recursiveKey,
                    $validator,
                    $errors,
                    $code,
                    $level++
                );
            }
            $index++;
        }
    }

}