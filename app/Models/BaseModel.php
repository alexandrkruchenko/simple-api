<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

abstract class BaseModel extends Model {

    /**
     * Relations for validation
     * @var array
     */
    public static $relationsForValidation = [];

    /**
     * Check on exist relation entity
     * @param array $data
     * @param array $relations
     * @return array
     */
    public static function validateRelations($data, $relations)
    {
        $errors = [];
        foreach ($relations as $key => $model) {
            foreach ($data as $dkey => $value) {
                if ($key == $dkey) {
                    $entity = $model::find($value);
                    $entityName = getEntityNameByClass($model);
                    if (!$entity) {
                        $errors[$dkey] = [
                            'database' => $entityName.' with id='.$value.' not found'
                        ];
                    }
                }
            }
        }
        return $errors;
    }

}