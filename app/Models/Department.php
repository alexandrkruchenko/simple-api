<?php
namespace App\Models;

class Department extends BaseModel {

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    public $fillable = ['name', 'parent_id'];

    public $timestamps = false;

    /**
     * Model relations
     * @var array
     */
    public static $relationsForValidation = [
        'parent_id' => \App\Models\Department::class
    ];

    /**
     * Entity creation validation rules
     * @var array
     */
    public static $onCreateValidationRules = [
        'name' => 'required',
        'parent_id' => 'numeric'
    ];

    /**
     * Entity update validation rules
     * @var array
     */
    public static $onUpdateValidationRules = [
        'parent_id' => 'numeric'
    ];

    /**
     * Get department employes
     */
    public function employes()
    {
        return $this->hasMany('App\Models\Employe');
    }

    /**
     * Get parents departments
     */
    public function parent()
    {
        return $this->belongsTo('App\Models\Department', 'parent_id');
    }

    /**
     * Get children departments
     */
    public function children()
    {
        return $this->hasMany('App\Models\Department', 'parent_id');
    }

    /**
     * Loads all descendants
     */
    public function subDepartments()
    {
        return $this->children()->with('subDepartments');
    }

}