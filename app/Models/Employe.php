<?php
namespace App\Models;

class Employe extends BaseModel {

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    public $fillable = ['firstname', 'lastname', 'department_id'];

    public $timestamps = false;

    /**
     * Model relations
     * @var array
     */
    public static $relationsForValidation = [
        'department_id' => \App\Models\Department::class
    ];

    /**
     * Entity creation validation rules
     * @var array
     */
    public static $onCreateValidationRules = [
        'firstname' => 'required|alpha|min:3',
        'lastname' => 'required|alpha|min:3',
        'department_id' => 'numeric'
    ];

    /**
     * Entity update validation rules
     * @var array
     */
    public static $onUpdateValidationRules = [
        'firstname' => 'alpha|min:3',
        'lastname' => 'alpha|min:3',
        'department_id' => 'numeric'
    ];
}