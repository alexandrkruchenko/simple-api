<?php
namespace App\Request;

class JsonRequest extends Request {
    
    /**
     * Return json parser
     * @return JsonRequestParser
     */
    public function getRequestParser(): RequestParserInterface
    {
        return new JsonRequestParser();
    }
}