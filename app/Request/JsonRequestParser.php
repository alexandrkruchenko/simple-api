<?php
namespace App\Request;

class JsonRequestParser implements RequestParserInterface {

    /**
     * Parse request
     * @param string $data
     * @return array
     */
    public function parse($data)
    {
        return json_decode($data, true);
    }

}