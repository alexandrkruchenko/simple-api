<?php
namespace App\Request;

abstract class Request {
    
    abstract public function getRequestParser(): RequestParserInterface;

    /**
     * Return request parser by format
     * @param string $type
     * @return JsonRequest|XmlRequest
     */
    public static function getRequestTypeByFormat($type)
    {
        $request = null;
        switch($type) {
            
            case 'json':
                $request = new JsonRequest();
            break;

            case 'xml':
                $request = new XmlRequest();
            break;

            default:
                \App\Response\Response::empty(415);
            break;
        }
        return $request;
    }

    /**
     * Parse request to array
     * @param string $data
     * @return array
     */
    public function toArray($data)
    {
        $decoder = $this->getRequestParser();
        return $decoder->parse($data);
    }
}