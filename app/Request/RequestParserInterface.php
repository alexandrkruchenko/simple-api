<?php
namespace App\Request;

interface RequestParserInterface {

    /**
     * Parse request
     * @param string $data
     * @return array
     */
    public function parse($data);
}