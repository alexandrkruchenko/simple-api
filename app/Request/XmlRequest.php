<?php
namespace App\Request;

class XmlRequest extends Request {
    
    /**
     * Return xml parser
     * @return XmlRequestParser
     */
    public function getRequestParser(): RequestParserInterface
    {
        return new XmlRequestParser();
    }
}