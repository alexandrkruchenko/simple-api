<?php
namespace App\Request;

class XmlRequestParser implements RequestParserInterface {

    /**
     * Parse request
     * @param string $data
     * @return array
     */
    public function parse($data)
    {
        $xml = @simplexml_load_string($data, 'SimpleXMLElement', LIBXML_NOCDATA);
        if ($xml) {
            $json = json_encode($xml);
            $array = @json_decode($json, true);
            if ($array) {
                $result = [];
                $this->clearItemWrapper($array, $result);
                if (array_key_exists('item', $result)) {
                    $result = $result['item'];
                    if (!array_key_exists(0, $result)) {
                        $result = [$result];
                    }
                }
                return $result;
            }
        }
        return [];
    }

    /**
     * Remove <item> node from $array
     * @param array $array
     * @param array $result
     */
    protected function clearItemWrapper($array, &$result)
    {
        foreach ($array as $key => $value) {
            if (is_array($value)) {
                if (array_key_exists('item', $value)) {
                    if (array_key_exists(0, $value['item'])) {
                        $value = $value['item'];
                    } else {
                        $value = [$value['item']];
                    }
                }
                $this->clearItemWrapper($value, $result[$key]);
            } else {
                $result[$key] = $value;
            }
        }
    }

}