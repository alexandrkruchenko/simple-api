<?php
namespace App\Response;

class JsonResponse extends Response {
    
    /**
     * Return encoder
     * @return JsonResponseEncoder
     */
    public function getResponseEncoder(): ResponseEncoderInterface
    {
        return new JsonResponseEncoder();
    }
}