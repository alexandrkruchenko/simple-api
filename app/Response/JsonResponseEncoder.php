<?php
namespace App\Response;

class JsonResponseEncoder implements ResponseEncoderInterface {

    public $contentType = 'application/json;';
    public $extension = 'json';

    /**
     * Form encoded response
     * @param array $data
     * @param array $errors
     * @return string
     */
    public function formApiResponse($data, $errors = [])
    {
        return $this->encode([
            'success' => empty($errors),
            'data'   =>  $data,
            'errors' => $errors
        ]);
    }

    /**
     * Encode to json
     * @param array $data
     * @return string
     */
    public function encode($data)
    {
        return json_encode($data);
    }

}