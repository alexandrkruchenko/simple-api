<?php
namespace App\Response;

abstract class Response {

    abstract public function getResponseEncoder(): ResponseEncoderInterface;

    /**
     * Return http message by code
     * @params integer $code
     * @param $code
     * @return string
     */
    public static function getHttpHeaderMessageByCode($code)
    {
        $httpMessages = [
            200 => $_SERVER['SERVER_PROTOCOL'].' 200 OK',
            201 => $_SERVER['SERVER_PROTOCOL'].' 201 Created',
            204 => $_SERVER['SERVER_PROTOCOL'].' 204 No Content',
            404 => $_SERVER['SERVER_PROTOCOL'].' 404 Not Found',
            415 => $_SERVER['SERVER_PROTOCOL'].' 415 Unsupported Media Type',
            422 => $_SERVER['SERVER_PROTOCOL'].' 422 Unprocessable Entity',
        ];
        return $httpMessages[$code] ?? $httpMessages[200];
    }

    /**
     * Return response type by format
     * @param string $type
     * @return JsonResponse|XmlResponse
     */
    public static function getResponseTypeByFormat($type)
    {
        $response = null;
        switch($type) {
            
            case 'json':
                $response = new JsonResponse();
            break;

            case 'xml':
                $response = new XmlResponse();
            break;

            default:
                Response::empty(415);
            break;
        }
        return $response;
    }

    /**
     * Return empty response
     * @param int $code
     */
    public static function empty($code = 200)
    {
        header(self::getHttpHeaderMessageByCode($code));
        exit;
    }

    /**
     * Send result
     * @param array $data
     * @param array $errors
     * @param integer $code
     * @return string
     */
    public function send($data, $errors = [], $code = 200)
    {
        $encoder = $this->getResponseEncoder();
        header(self::getHttpHeaderMessageByCode($code));
        header('Content-Type: '.$encoder->contentType);
        echo $encoder->formApiResponse($data, $errors);
    }

    /**
     * Send result
     * @param array $data
     * @return string
     */
    public function file($data)
    {
        $encoder = $this->getResponseEncoder();
        $file = $encoder->encode($data);
        header(self::getHttpHeaderMessageByCode(200));
        header('Cache-Control: public');
        header('Content-Type: '.$encoder->contentType);
        header('Content-Transfer-Encoding: Binary');
        header('Content-Length: '.mb_strlen($file));
        header('Content-Disposition: attachment; filename=export.'.$encoder->extension);
        echo $file;
        exit;
    }

}