<?php
namespace App\Response;

interface ResponseEncoderInterface {
    
    /**
     * Form encoded response
     * @param array $data
     * @param array $errors
     * @return string
     */
    public function formApiResponse($data, $errors = []);

    /**
     * Encode result
     * @param array $data
     * @return string
     */
    public function encode($data);
}