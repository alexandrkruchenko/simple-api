<?php
namespace App\Response;

class XmlResponse extends Response {
    
    /**
     * Return encoder
     * @return XmlResponseEncoder
     */
    public function getResponseEncoder(): ResponseEncoderInterface
    {
        return new XmlResponseEncoder();
    }
}