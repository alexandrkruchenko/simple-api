<?php
namespace App\Response;

class XmlResponseEncoder implements ResponseEncoderInterface {
    
    public $contentType = 'application/xml;';
    public $extension = 'xml';

    /**
     * Form encoded response
     * @param array $data
     * @param array $errors
     * @return string
     */
    public function formApiResponse($data, $errors = [])
    {
        $succes = empty($errors);

        //create dom
        $dom = new \DOMDocument('1.0');
        $dom->encoding = 'utf-8';
        $dom->xmlVersion = '1.0';
        $dom->formatOutput = true;

        //create xml root node
        $root = new \SimpleXMLElement('<root/>');

        //create success node
        $successNode = $root->addChild('success', (int) $succes);

        //create data node
        $dataNode = $root->addChild('data');
        $this->arrayToXml($data, $dataNode);

        //create errors node
        $errorsNode = $root->addChild('errors');
        $this->arrayToXml($errors, $errorsNode);

        $dom->loadXML($root->asXML());
        return $dom->saveXML(); 
    }

    /**
     * Encode to xml
     * @param array $data
     * @return string
     */
    public function encode($data)
    {
        //create dom
        $dom = new \DOMDocument('1.0');
        $dom->encoding = 'utf-8';
        $dom->xmlVersion = '1.0';
        $dom->formatOutput = true;

        $root = new \SimpleXMLElement('<root/>');
        $this->arrayToXml($data, $root);

        $dom->loadXML($root->asXML());
        return $dom->saveXML(); 
    }

    /**
     * Convert Array to xml
     * @param array $array
     * @param \SimpleXMLElement $xml
     * @return \SimpleXMLElement $xml
     */
    protected function arrayToXml($array, &$xml) {      
        foreach($array as $key => $value) {
            $key = is_numeric($key) ? 'item' : $key;  
            if(is_array($value)) {
                $subnode = $xml->addChild($key);
                $this->arrayToXml($value, $subnode);
            } else {
                $xml->addChild($key, $value);
            }
        }        
    }
}