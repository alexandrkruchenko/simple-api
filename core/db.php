<?php 
use Illuminate\Database\Capsule\Manager as Capsule;

$driver    = getenv('DB_DRIVER');
$host      = getenv('DB_HOST');
$charset   = getenv('DB_CHARSET');
$collation = getenv('DB_COLLATION');
$prefix    = getenv('DB_PREFIX');

$capsule = new Capsule;
$capsule->addConnection([
    'driver'    => $driver ? $driver : 'mysql',
    'host'      => $host ? $host : 'localhost',
    'database'  => getenv('DB_DATABASE'),
    'username'  => getenv('DB_USERNAME'),
    'password'  => getenv('DB_PASSWORD'),
    'charset'   => $charset ? $charset : 'utf8',
    'collation' => $collation ? $collation : 'utf8_unicode_ci',
    'prefix'    => $prefix ? $prefix : ''
]);
$capsule->setAsGlobal();
$capsule->bootEloquent();