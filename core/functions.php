<?php

/**
 * Get entity by class name
 * @param string $class
 * @return string
 */
function getEntityNameByClass($class)
{
    return substr(strrchr($class, "\\"), 1);
}