<?php

require_once 'vendor/autoload.php';
require_once 'core/env.php';
require_once 'core/db.php';

use Illuminate\Database\Capsule\Manager as Capsule;

$statement = <<<EOS

    CREATE TABLE IF NOT EXISTS departments (
        id INT NOT NULL AUTO_INCREMENT,
        name VARCHAR(100) NOT NULL,
        parent_id INT,
        FOREIGN KEY (parent_id)
            REFERENCES departments(id)
            ON DELETE SET NULL,
        PRIMARY KEY (id)
    )  ENGINE=INNODB;

    CREATE TABLE IF NOT EXISTS employes (
        id INT NOT NULL AUTO_INCREMENT,
        firstname VARCHAR(100) NOT NULL,
        lastname VARCHAR(100) NOT NULL,
        department_id INT,
        FOREIGN KEY (department_id)
            REFERENCES departments(id)
            ON DELETE SET NULL,
        PRIMARY KEY (id)
    )  ENGINE=INNODB;

    INSERT INTO departments
        (name)
    VALUES
        ('A'),
        ('B'),
        ('C'),
        ('D'),
        ('E'),
        ('E'),
        ('G');
    
    UPDATE departments SET parent_id = 1 WHERE id IN(2, 3, 5, 7);
    UPDATE departments SET parent_id = 2 WHERE id = 3;
    UPDATE departments SET parent_id = 3 WHERE id = 4;
    UPDATE departments SET parent_id = 4 WHERE id = 6;
 

    INSERT INTO employes
        (firstname, lastname, department_id)
    VALUES
        ('Krasimir', 'Hristozov', 1),
        ('Maria', 'Petrova', 2),
        ('Masha', 'Ivanova', 3),
        ('Jane', 'Smith', 4),
        ('John', 'Johnson', 5),
        ('Richard', 'Smith', 6),
        ('Donna', 'Smith', 7),
        ('Josh', 'Sidorov', 3),
        ('Anna', 'Henderson', 6);
EOS;

Capsule::unprepared($statement);