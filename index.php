<?php

/**
 * Simple Api
 */

//add composer
require_once 'vendor/autoload.php';

//init env
require_once 'core/env.php';

//init database
require_once 'core/db.php';

require_once 'core/functions.php';

//init routes
$router = new \Buki\Router([
    'paths' => [
        'controllers' => 'app/Controllers/Api',
    ],
    'namespaces' => [
        'controllers' => 'App\Controllers\Api',
    ],
]);

require_once 'routes.php';

$router->run();