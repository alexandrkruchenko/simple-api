<?php

$router->group('api', function($r) {

    //employe
    $r->get('employes.:string', 'EmployeController@index');
    $r->get('employes/:id.:string', 'EmployeController@show');
    $r->post('employes.:string', 'EmployeController@create');
    $r->put('employes/:id.:string', 'EmployeController@update');
    $r->delete('employes/:id.:string', 'EmployeController@destroy');
    
    //department
    $r->get('departments.:string', 'DepartmentController@index');
    $r->get('departments/:id.:string', 'DepartmentController@show');
    $r->post('departments.:string', 'DepartmentController@create');
    $r->put('departments/:id.:string', 'DepartmentController@update');
    $r->delete('departments/:id.:string', 'DepartmentController@destroy');

    //structure
    $r->get('departments/:string/structure.:string', 'DepartmentController@structure');

    //import
    $r->post('departments/:string/import.:string', 'DepartmentController@import');
});

$router->error(function() {
    \App\Response\Response::empty(404);
});